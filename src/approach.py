from process   import preprocess
from process   import process
from graph     import preprocess_neo4j


''' The main function '''
def main():

# Common Processing
	# clusters
	preprocess()
	process()

# Processing for Graph Analysis
	preprocess_neo4j()



if __name__ == '__main__':
	main()