import sys
import pandas as pd
from fastdtw import fastdtw
from scipy.spatial.distance import euclidean

# read file
df = pd.read_csv(sys.argv[1])

# preprocess by transforming string to list of ints
df['Sizes'] = [[int(n) for n in x.strip('[]').split(',')] for x in df['Sizes']]

# run fastdtw
n = len(df['Sizes'])
nComparisons = 0

# optimized < n^2 comparisons
distances=[]
for i in range(n):
	row = df['Sizes'][i]
	for j in range(n):
		if (j >= i): continue
		column = df['Sizes'][j]
		distance = 0
		distance = fastdtw(row, column)[0]; nComparisons = nComparisons + 1
		distances.append([df['Path'][i],df['Path'][j],str(row),str(column),distance])

d = pd.DataFrame(distances)
d.columns = ['Path1','Path2','Size1','Size2','Distance']

# Case Study 3 Results
print('Number of cluster evolution paths           : ' + str(n))
print('Number of cluster evolution path comparisons: ' + str(nComparisons))
print('Sum of distance values                      : ' + str(d['Distance'].sum()))
print('Similarity value                            : ' + str(nComparisons/d['Distance'].sum()))