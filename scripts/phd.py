import os.path, glob

from qgis.core import (QgsProject,
					   QgsVectorLayer,
					   QgsVectorLayerTemporalProperties,
					   QgsDateTimeRange,
					   QgsInterval,
					   QgsUnitTypes,
					   QgsAnnotationLayer,
					   QgsCoordinateTransformContext,
					   QgsSimpleMarkerSymbolLayer,
					   QgsFontMarkerSymbolLayer,
					   QgsMarkerSymbol,
					   QgsAnnotationMarkerItem,
					   QgsPoint,
					   QgsPointXY,
					   QgsSimpleMarkerSymbolLayer)
from qgis.gui import QgisInterface
from qgis.utils import iface
 
from qgis.PyQt.QtGui import QColor
from qgis.PyQt.QtCore import QPointF


# Imports clusters

def importClusters():

	base = '/path/to/folder/'
	cluster = '*'
	txt = '*.txt'

	folder = base + cluster + '/' + txt
	root   = QgsProject.instance().layerTreeRoot()

	color = {
		0: QColor(  0,156, 59),
		1: QColor(255,223,  0),
		2: QColor(  0, 39,118),
		3: QColor( 84,176, 74),
		4: QColor(240,240, 35),
		5: QColor( 33,133,135),
		6: QColor(  0,125,  0),
		7: QColor(170,170,  0),
		8: QColor( 45, 75,255),
		9: QColor(177,255,177),
		10: QColor(177,255,255),
		11: QColor(255,255,177)
	}

	globalStartTime = None
	globalEndTime   = None

	for i, file in enumerate(glob.glob(folder)):

		groupName = os.path.basename(os.path.dirname(file))
		groupNode = root.findGroup(groupName)

		if(groupNode == None):
			groupNode = root.addGroup(groupName)
			groupNode.setItemVisibilityCheckedRecursive(False)
			groupNode.setExpanded(False)

		uri = 'file:///' + file + '?type=csv&xField=lon&yField=lat&spatialIndex=no&subsetIndex=no&watchFile=no&crs=epsg:4326'

		vlayer = QgsVectorLayer(uri, os.path.basename(file), 'delimitedtext')

		vlayer.setFieldAlias(0,'id')
		vlayer.setFieldAlias(1,'date')
		vlayer.setFieldAlias(2,'lat')
		vlayer.setFieldAlias(3,'lon')

		vlayer.renderer().symbol().setSize(4)
		vlayer.renderer().symbol().setColor(color[i%len(color)])

		vlayer.temporalProperties().setIsActive(True)
		vlayer.temporalProperties().setMode(QgsVectorLayerTemporalProperties.ModeFeatureDateTimeInstantFromField)
		vlayer.temporalProperties().setStartField('date')

		QgsProject.instance().addMapLayer(vlayer, False)
		groupNode.addLayer(vlayer)

		time = vlayer.getFeature(2).attribute(1)
		if(globalStartTime == None): globalStartTime = time
		if(globalEndTime   == None): globalEndTime   = time
		if(time < globalStartTime): globalStartTime  = time
		if(time > globalEndTime)  : globalEndTime    = time

	iface.mapCanvas().temporalController().setTemporalExtents(QgsDateTimeRange(globalStartTime,globalEndTime))
	iface.mapCanvas().temporalController().setFrameDuration(QgsInterval(1,QgsUnitTypes.TemporalMinutes))





	folder = base + cluster + '/cluster.clu'
	cstart = []
	cend   = []
	for i in range(0,len(glob.glob(folder))):
		cstart.append(QgsAnnotationLayer('start', QgsAnnotationLayer.LayerOptions(QgsCoordinateTransformContext())))
		cend.append(QgsAnnotationLayer('end', QgsAnnotationLayer.LayerOptions(QgsCoordinateTransformContext(QgsProject.instance().transformContext()))))

	for i, file in enumerate(glob.glob(folder)):
		clusterId = os.path.basename(os.path.dirname(file))

		f = open(file, 'r')
		cstartlat = f.readline()
		cstartlon = f.readline()
		cendlat   = f.readline()
		cendlon   = f.readline()
		f.close()



		sCircle = QgsSimpleMarkerSymbolLayer()
		sCircle.setSize(3)
		sCircle.setColor(QColor(0,255,0))

		sText = QgsFontMarkerSymbolLayer()
		sText.setCharacter(clusterId)
		sText.setFontFamily('Helvetica')
		sText.setFontStyle('Regular')
		sText.setSize(3)
		sText.setColor(QColor(0,0,0))
		sText.setOffset(QPointF(0,4))

		sMarker = QgsMarkerSymbol()
		sMarker.appendSymbolLayer(sCircle)
		sMarker.appendSymbolLayer(sText)

		sAnnotationItem = QgsAnnotationMarkerItem(QgsPoint(QgsPointXY(float(cstartlon), float(cstartlat))))
		sAnnotationItem.setSymbol(sMarker.clone())

		cstart[i].addItem(sAnnotationItem.clone())


		eCircle = QgsSimpleMarkerSymbolLayer()
		eCircle.setSize(3)
		eCircle.setColor(QColor(0,0,0))

		eText = QgsFontMarkerSymbolLayer()
		eText.setCharacter(clusterId)
		eText.setFontFamily('Helvetica')
		eText.setFontStyle('Regular')
		eText.setSize(3)
		eText.setColor(QColor(0,0,0))
		eText.setOffset(QPointF(0,4))

		eMarker = QgsMarkerSymbol()
		eMarker.appendSymbolLayer(eCircle)
		eMarker.appendSymbolLayer(eText)

		eAnnotationItem = QgsAnnotationMarkerItem(QgsPoint(QgsPointXY(float(cendlon), float(cendlat))))
		eAnnotationItem.setSymbol(eMarker.clone())

		cend[i].addItem(eAnnotationItem.clone())



		groupNode = root.findGroup(clusterId)
		groupNode.addLayer(cstart[i])
		groupNode.addLayer(cend[i])
		QgsProject.instance().addMapLayer(cstart[i], False)
		QgsProject.instance().addMapLayer(cend[i], False)



def showAll():
	root = QgsProject.instance().layerTreeRoot()
	for cluster in root.children():
		cluster.setItemVisibilityCheckedRecursive(True)

def hideAll():
	root = QgsProject.instance().layerTreeRoot()
	for cluster in root.children():
		cluster.setItemVisibilityCheckedRecursive(False)



def showCluster(*id):
	showTrajectories(*id)
	showStartEnd(*id)

def showClusters(*id):
	showCluster(*id)

def hideCluster(*id):
	hideTrajectories(*id)
	hideStartEnd(*id)

def hideClusters(*id):
	hideCluster(*id)



def show_(*args, start: bool, end: bool):
	root = QgsProject.instance().layerTreeRoot()
	ids = [str(i) for i in args] if args else [c.name() for c in root.children()]
	for cluster in root.children():
		if cluster.name() in ids:
			for trajectory in cluster.children():
				if start and trajectory.name() == 'start':
					trajectory.setItemVisibilityCheckedParentRecursive(True)
				elif end and trajectory.name() == 'end':
					trajectory.setItemVisibilityCheckedParentRecursive(True)
				elif not start and not end and trajectory.name() != 'start' and trajectory.name() != 'end':
					trajectory.setItemVisibilityCheckedParentRecursive(True)

def hide_(*args, start: bool, end: bool):
	root = QgsProject.instance().layerTreeRoot()
	ids = [str(i) for i in args] if args else [c.name() for c in root.children()]
	for cluster in root.children():
		if cluster.name() in ids:
			for trajectory in cluster.children():
				if start and trajectory.name() == 'start':
					trajectory.setItemVisibilityChecked(False)
				elif end and trajectory.name() == 'end':
					trajectory.setItemVisibilityChecked(False)
				elif not start and not end and trajectory.name() != 'start' and trajectory.name() != 'end':
					trajectory.setItemVisibilityChecked(False)


def showTrajectories(*ids):
	show_(*ids, start=False, end=False)

def hideTrajectories(*ids):
	hide_(*ids, start=False, end=False)

def showStart(*ids):
	show_(*ids, start=True, end=False)

def hideStart(*ids):
	hide_(*ids, start=True, end=False)

def showEnd(*ids):
	show_(*ids, start=False, end=True)

def hideEnd(*ids):
	hide_(*ids, start=False, end=True)

def showStartEnd(*ids):
	show_(*ids, start=True, end=True)

def hideStartEnd(*ids):
	hide_(*ids, start=True, end=True)
